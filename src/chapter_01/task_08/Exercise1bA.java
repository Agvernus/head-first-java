package chapter_01.task_08;

class Exercise1bA {
	public static void main(String[] args) {
		int x = 1;
		while (x < 10) {
			x = x + 1; //to prevent an infinite 'while' loop
			if (x > 3) {
				System.out.println("big x");
			}
		}
	}
}
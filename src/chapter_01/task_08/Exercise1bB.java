package chapter_01.task_08;

class Exercise1bB {
	public static void main(String[] args) {
		int x = 5;
		while (x > 1) {
			x = x - 1; //to prevent an infinite 'while' loop
			if (x < 3) {
				System.out.println("small x");
			}
		}
	}
}
declare a string of characters variable named 'name' and give it the value "Fido"
declare a new Dog variable 'myDog' and make the new Dog using 'name' and 'size'
subtract 5 from 27 (value of 'size') and assign it to a variable named 'x'
if x (value of 22) is less than 15, tell the dog to bark 8 times

keep looping as long as x is greater than 3...
tell the dog to play (whatever THAT means to a dog...)
this looks like the end of the loop - everything in {} is done in the loop

declare a list of integers variable 'numList', and put 2,4,6,8 into the list
print out "Hello" ... probably at the command-line
print out "Dog: Fido" (the value of 'name' is "Fido") at the command-line
declare a character string variable 'num' and give it the value of "8"

try to do something... maybe the thing we're trying isn't guaranteed to work...
read a text file named "myFile.txt" (or at least TRY to read the file...)
must be the end of the "things to try", so I guess you could try many things...
this must be where you find out if the thing you tried didn't work...
if the thing we tried failed, print "File not found" out at the command-line
looks like everything in the {} is what to do if the 'try' didn't work
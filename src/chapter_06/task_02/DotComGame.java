package chapter_06.task_02;

import java.util.ArrayList;

public class DotComGame {
	
	public static void main(String[] args) {
		int numOfGuesses = 0;
		GameHelper helper = new GameHelper();
		
		DotCom dotCom = new DotCom();
		int randomNum = (int) (Math.random() * 5);
		
		ArrayList<String> locations = new ArrayList<>() {{add(randomNum + ""); add(randomNum + 1 + ""); add(randomNum + 2 + "");}};
		dotCom.setLocationCells(locations);
		boolean isAlive = true;
		
		while (isAlive == true) {
			String guess = helper.getUserInput("enter a number");
			String result = dotCom.checkYourself(guess);
			numOfGuesses++;
			System.out.println(result);
			if (result.equals("kill")) {
				isAlive = false;
				System.out.println("You took " + numOfGuesses + " guesses");
			} // close if
		} // close while
	} // close main
}
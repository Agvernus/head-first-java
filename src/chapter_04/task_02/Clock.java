package chapter_04.task_02;

class Clock {
	private String time;
	
	public void setTime(String t) {
		time = t;
	}
	
	public String getTime() {
		return time;
	}
}